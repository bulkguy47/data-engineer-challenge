# HiPages Data Engineer Tech Challenge Solution by Badruddin Kamal(27-11-2020)
The Data received by the server in this case is in json format. The script reads the data and generates the activity report and timebucket report from it.

## Process
- The Json file is read into a dataframe.
- The dataframe file is flattened.
```
------- Original File Structure -------
 |-- action: string (nullable = true)
 |-- event_id: string (nullable = true)
 |-- timestamp: string (nullable = true)
 |-- url: string (nullable = true)
 |    |-- id: long (nullable = true)
 |    |-- ip: string (nullable = true)
 |    |-- session_id: string (nullable = true)

------- Flattened File Structure -------
 |-- action: string (nullable = true)
 |-- event_id: string (nullable = true)
 |-- timestamp: string (nullable = true)
 |-- url: string (nullable = true)
 |-- id: long (nullable = true)
 |-- ip: string (nullable = true)
 |-- session_id: string (nullable = true)
```
- The flattened dataframe add other dimension data such as time bucket and url level information.
- Then we generate the time_bucket and activity report in csv and parquet format, with corresponding metrics.
Example results:

| user_id | time_stamp | url_level1 | url_level2 | url_level3 | activity | 
| --- | --- | --- | --- | --- | --- | 
| 564561 | 02/02/2017 20:22:00 | hipages.com | articles | NULL | page_view | 
| 564561 | 02/02/2017 20:23:00 | hipages.com | connect | sfobusiness | button_click | 


| time_bucket | url_level1 | url_level2 | activity | activity_count | user_count |
| --- | --- | --- | --- | --- | --- |
| 2017020220 | hipages.com | articles | page_view | 4 | 1 | 
| 2017020220 | hipages.com | connect | button_click | 2 | 1 | 

## Dependencies
The choice of Python and Spark has to do with being able easily deploy in distributed environments.  If we further want to add more scalability we can divide each step to make the process further scalable.
- Python 3.8.0
- Spark 3.0.1
- pyspark 3.0.1

## Running tests
```
python -m unittest
```

## Running the script
Manually:
```
python .\process_file.py source_event_data.json
```
Ideally it should be triggered automatically by an Airflow or CronJob externally.

Note: The script is set to run on a local spark context, if you were to deploy it on a different spark system, please change the details in line 14 of `process_file.py` 

## Output
Currently we process the data and generate a csv and parquet. Parquets are an interesting format of data that allow us to query over them using specific tools(Athena) or load them into different data warehouse tools (BigQuery) and at the sametime archive as well as share them more easily and affectively.


