import unittest
from process_file import *

class TestGetTimeBucket(unittest.TestCase):

    def test_get_time_bucket_none(self):
        assert get_time_bucket(None) == "0000000000", "Should be 0000000000"

    def test_get_time_bucket_date(self):
        assert get_time_bucket("01/01/2020 12:00:01") == "2020010112", "Should be 2020010112"

class TestGetURILevel(unittest.TestCase):

    def test_get_url_level1_none(self):
        assert get_url_level1(None) == "NULL", "Should be NULL"

    def test_get_url_level2_none(self):
        assert get_url_level2(None) == "NULL", "Should be NULL"

    def test_get_url_level3_none(self):
        assert get_url_level3(None) == "NULL", "Should be NULL"

    def test_get_url_level1_uri(self):
        assert get_url_level1("https://www.programiz.com/python-programming/methods/string?a=123") == "programiz.com", "Should be programiz.com"

    def test_get_url_level2_uri(self):
        assert get_url_level2("https://www.programiz.com/python-programming/methods/string?a=123") == "python-programming", "Should be python-programming"

    def test_get_url_level3_uri(self):
        assert get_url_level3("https://www.programiz.com/python-programming/methods/string?a=123") == "methods", "Should be methods"
    
    def test_get_url_level_none(self):
        assert get_url_level(None,1) == "NULL", "Should be NULL"

    def test_get_url_level_uri(self):
        assert get_url_level("https://www.programiz.com/python-programming/methods/string?a=123",1) == "programiz.com", "Should be programiz.com"

    def test_get_url_level_uri_incorrect(self):
        assert get_url_level("https://www.programiz.com/python-programming/methods/string?a=123",20) == "NULL", "Should be NULL"

if __name__ == '__main__':
    unittest.main()