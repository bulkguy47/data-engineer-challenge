#Badruddin Kamal
#This Spark scipt will read hipages json data and generate csv and parquet reports of activity and time_bucket

from pyspark.sql.types import StructType
from pyspark.sql.functions import col
from pyspark.sql import SparkSession
from pyspark.sql.functions import udf
from pyspark.sql.types import StringType,IntegerType
import os
from datetime import datetime
import sys

def init_spark():
  spark = SparkSession.builder.appName("HiPages").master("local[*]").config("spark.driver.host","127.0.0.1").config("spark.driver.bindAddress","127.0.0.1").getOrCreate()
  sc = spark.sparkContext
  return spark,sc

# src - https://stackoverflow.com/questions/63757221/how-to-flatten-json-file-in-pyspark
# return a list of all (possibly nested) fields to select, within a given schema
def flatten(schema, prefix: str = ""):
    # return a list of sub-items to select, within a given field
    def field_items(field):
        name = f'{prefix}.{field.name}' if prefix else field.name
        if type(field.dataType) == StructType:
            return flatten(field.dataType, name)
        else:
            return [col(name)]
    return [item for field in schema.fields for item in field_items(field)]

def read_file_and_flatten(file,spark):
    df = spark.read.json(file)
    print("------- Original File Structure -------")
    df.printSchema()
    flattened = flatten(df.schema)
    df2 = df.select(*flattened)
    print("------- Flattened File Structure -------")
    df2.printSchema()
    return df2.withColumnRenamed('id', 'user_id').withColumnRenamed('action', 'activity')

def get_time_bucket(timestamp):
    if (timestamp != timestamp) | (timestamp is None):
       return "0000000000"
    else:
        return datetime.strptime(timestamp, '%d/%m/%Y %H:%M:%S').strftime('%Y%m%d%H')

def get_url_level(url,level):
    if (url != url) | (url is None):
       return "NULL"
    else:
        try:
            return url.lower().replace('http://','').replace('https://','').replace('www.','').split('?', 1)[0].split('/')[level-1]
        except:
            return "NULL"

def get_url_level1(url):
    return get_url_level(url,1)

def get_url_level2(url):
    return get_url_level(url,2)

def get_url_level3(url):
    return get_url_level(url,3)

def main():
    print("Processing: {}".format(sys.argv[1]))
    spark,sc = init_spark()
    df=read_file_and_flatten(sys.argv[1],spark)
    filename=sys.argv[1].replace('.','_')

    # Generate other dimension fields
    time_bucket_func = udf(get_time_bucket,StringType()) 
    df = df.withColumn("time_bucket",time_bucket_func(df.timestamp)) 
    url_level1_func = udf(get_url_level1,StringType())
    df = df.withColumn("url_level1",url_level1_func(df.url)) 
    url_level2_func = udf(get_url_level2,StringType())
    df = df.withColumn("url_level2",url_level2_func(df.url)) 
    url_level3_func = udf(get_url_level3,StringType())
    df = df.withColumn("url_level3",url_level3_func(df.url)) 

    # Generate report and calculated fields
    print("Generating Reports")
    dt=datetime.now().strftime('%Y%m%d%H%M%S')
    activity_report=df.select('user_id',df['timestamp'],df['url_level1'],df['url_level2'],df['url_level3'],df['activity'])
    activity_report.coalesce(1).write.format('csv').option('header',True).mode('overwrite').option('sep',',').save("output/csv/{}/{}_activity_report_{}".format(dt,filename,dt))
    activity_report.coalesce(1).write.parquet("output/parquet/{}/{}_activity_report_{}".format(dt,filename,dt))

    time_bucket_user_activity=df.select('time_bucket',df['url_level1'],df['url_level2'],df['user_id'],df['activity']).groupBy('time_bucket','url_level1','url_level2','user_id','activity').count().withColumnRenamed('count', 'activity_count').cache()
    time_bucket_activity=time_bucket_user_activity.select('time_bucket',time_bucket_user_activity['url_level1'],time_bucket_user_activity['url_level2'],time_bucket_user_activity['activity'],time_bucket_user_activity['activity_count']).groupBy('time_bucket','url_level1','url_level2','activity').sum('activity_count').withColumnRenamed('sum(activity_count)', 'activity_count').cache()
    time_bucket_user=time_bucket_user_activity.select('time_bucket',time_bucket_user_activity['url_level1'],time_bucket_user_activity['url_level2'],time_bucket_user_activity['activity'],time_bucket_user_activity['user_id']).groupBy('time_bucket','url_level1','url_level2','activity').count().withColumnRenamed('count', 'user_count').cache()
    time_bucket_report=time_bucket_activity.join(time_bucket_user, on=['time_bucket','url_level1','url_level2','activity'], how='inner').cache()
    time_bucket_report.coalesce(1).write.format('csv').option('header',True).mode('overwrite').option('sep',',').save("output/csv/{}/{}_time_bucket_report_{}".format(dt,filename,dt))
    time_bucket_report.coalesce(1).write.parquet("output/parquet/{}/{}_time_bucket_report_{}".format(dt,filename,dt))
    print("Finished")

if __name__ == "__main__":
    main()